<?php
/*
Plugin Name: EyChat聊天极简版
Description: 为WordPress网站提供可创建组群聊天的即时通讯系统.
Author: eyoung 1507309730@qq.com
Version: 1.0
Author URI: https://wordpress.yuyaoit.net/
*/


if(!defined('ABSPATH')){
    return;
}

define('EYOUNG_CHAT_PATH',dirname(__FILE__));
define('EYOUNG_CHAT_FILE',__FILE__);
//define('EYOUNG_CHAT_JS', 'jsdev');
define('EYOUNG_CHAT_JS', 'dist');
define('EYOUNG_CHAT_URL',plugin_dir_url(EYOUNG_CHAT_FILE));

require_once EYOUNG_CHAT_PATH.'/classes/admin.class.php';
require_once EYOUNG_CHAT_PATH.'/classes/front.class.php';

EYOUNGCHAT_Admin::init();
EYOUNGCHAT_Front::init();