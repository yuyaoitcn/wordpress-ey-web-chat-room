<?php

class EYOUNGCHAT_Admin{

	public static $name = 'ey_chat';
	public static $optname = 'eychat_options';
	public static $api_reg = 'https://wordpress.yuyaoit.net';	
//	public static $api_reg = 'http://127.0.0.1:8787';
	public static $api_pay = 'https://service.yuyaoit.cn/wpplugin/index?app=ey-chat';	
	public static $role_ser = 99;
	public static $role_uer = 0;
	public static $role_ver = -1;
	public static $rid = 0;
	
	public function __construct(){}
	
	public static function retJson($data){
		header("Content-type: application/json;charset=utf-8");
		exit(json_encode($data));
	}

	public static function manageSetting(){
		$lang = EYOUNGCHAT_Front::$lg;
		if (!empty($_POST)){
			if(!isset($_POST["formhash"]) || $_POST["formhash"] !== wp_create_nonce()){
				$ret['code'] = 0;
				$ret['msg'] = 'post error!';
				self::retJson($ret);
			}
			
			$optdata['join'] = intval($_POST['join']);
			
			if (($optdata['join'] == '2') && (empty($_POST['join2']))){
				$ret['code'] = 0;
				$ret['msg'] = $lang['act_lost_join2'];
				self::retJson($ret);
			}
			
			$roles = get_editable_roles();

			if (!empty($_POST['join'.$optdata['join']])){
				if (is_array($_POST['join'.$optdata['join']])) {
					$_data = [];
					foreach ($_POST['join'.$optdata['join']] as $k => $v){
						if (isset($roles[$v['value']])){
							array_push($_data, $v['value']);
						}
					}
				}else{
					$_data = sanitize_key($_POST['join'.$optdata['join']]);
				}
				$optdata['join'.$optdata['join']] =  $_data;
			}
			
			$optdata['allowtalk'] = intval($_POST['allowtalk']);
			
			if (!empty($_POST['allowtalk'.$optdata['allowtalk']])){
				if (is_array($_POST['allowtalk'.$optdata['allowtalk']])) {
					$_data = [];
					foreach ($_POST['allowtalk'.$optdata['allowtalk']] as $k => $v){
						if (isset($roles[$v['value']])){
							array_push($_data, $v['value']);
						}
					}
				}else{
					$_data = sanitize_key($_POST['allowtalk'.$optdata['allowtalk']]);
				}
				$optdata['allowtalk'.$optdata['allowtalk']] =  $_data;
			}

			$optdata['title'] = wp_unslash(sanitize_text_field($_POST['title']));
			$optdata['notice'] = wp_unslash(sanitize_text_field($_POST['notice']));
			$optdata['join2circle'] = intval($_POST['join2circle']);
			$optdata['savechat']	= isset($_POST['savechat']) ? 1 : 0;
			$optdata['chatshow']	= isset($_POST['chatshow']) ? 1 : 0;
			$optdata['limitcontent'] = intval($_POST['limitcontent']);
			$optdata['limitspeed'] = intval($_POST['limitspeed']);
			$optdata['imageupload'] = isset($_POST['imageupload']) ? 1 : 0;
			$optdata['imagelimit'] = intval($_POST['imagelimit']);
			$optdata['imagenotvisitor'] = isset($_POST['imagenotvisitor']) ? 1 : 0;	
			$optdata['fileupload'] = isset($_POST['fileupload']) ? 1 : 0;
			$optdata['filelimit'] = intval($_POST['filelimit']);
			$optdata['filenotvisitor'] =  isset($_POST['filenotvisitor']) ? 1 : 0;
			$optdata['fileext'] = sanitize_text_field($_POST['fileext']);
			$optdata['maskwords'] = wp_unslash(sanitize_text_field($_POST['maskwords']));

			$_opt = get_option(self::$optname);
			foreach ($optdata as $k => $v) $_opt[$k] = $v;
			update_option(self::$optname, $_opt);
			$ret['code'] = 1;
			$ret['opt'] = $optdata;
			self::retJson($ret);
		}

		$_opt = get_option(self::$optname);
		
		$group = [];
		$roles = get_editable_roles();
		foreach ($roles as $k => $v) array_push($group, array('value'=>$k, 'title'=>wp_specialchars_decode(translate_user_role( $v['name']))));
		include_once(EYOUNG_CHAT_PATH . '/assets/admin.php');
	}
	
	public static function upSocketUrl(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {		
			$response = wp_remote_post(self::$api_reg.'/plugin/chat/upsocket',array('method' => 'POST','sslverify' => false,'body' => array('weburl' => site_url(),'webkey' => AUTH_KEY,'k' => AUTH_SALT,'v' => md5(AUTH_SALT.AUTH_KEY.site_url()))));
			if (is_wp_error( $response ) ) {
				$ret['code'] = 0;
				$ret['msg'] = $response->get_error_message();
				self::retJson($ret);
			} else {
				$_res = wp_remote_retrieve_body($response);
				$data = json_decode($_res, true);
				if ($data['code'] == '1'){
					$_opt = get_option(self::$optname);
					foreach ($data['data'] as $k => $v) $_opt[$k] = $v;
					update_option(self::$optname, $_opt);
					
					$data['data']['limitdate'] = date("Y-m-d H:i", $data['data']['limitdate']);
					$ret['code'] = 1;
					$ret['data'] = $data['data'];
					self::retJson($ret);
				}else{
					$ret['code'] = 0;
					$ret['msg'] = $data['msg'];
					self::retJson($ret);
				}
			}
		}
		
		$ret['code'] = 0;
		$ret['msg'] = 'post error!';
		self::retJson($ret);
	}
	
	public static function getMember(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {
			include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';
			$limit = empty($_POST['limit']) ? 10 : intval($_POST['limit']);
			$page = max(1, intval($_POST['page']));
			$data = EYOUNGCHAT_Roommember::get(null,'uid,lvl,stopspeak,stopenter,mtime', ($page - 1) * $limit, 10);
			$ret['code'] = 1;
			$ret['count'] = empty($_POST['count']) ? EYOUNGCHAT_Roommember::count() : intval($_POST['count']);
			if (!empty($data)){
				$ids = array_column($data,'uid');
				$_u = get_users( array('include'=>$ids, 'fields' => array( 'id','display_name' ) ) );
				$ret['member'] = array_column($_u,'display_name','id');
			}
			$ret['data'] = $data;
			self::retJson($ret);
		}
		
		$ret['code'] = 0;
		self::retJson($ret);
	}
	
	public static function setMember(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {
		
			if (!empty($_POST['memberids'])){
				include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';
				if (strpos($_POST['memberids'],',') !== false){
					$ids = explode(',', sanitize_text_field($_POST['memberids']));
				}else{
					$ids[] = sanitize_user($_POST['memberids']);
				}
				
				$_u = get_users( array('include'=>$ids, 'fields' => array( 'id' ) ) );
				
				foreach ($_u as $k => $v){
					 EYOUNGCHAT_Roommember::add(array('rid'=>self::$rid,'uid'=>$v->id,'lvl'=>0));
				}
				$ret['code'] = 1;
				$ret['msg'] = count($_u);
				self::retJson($ret);
			}
		}
		
		$ret['code'] = 0;
		self::retJson($ret);
	}
	
	public static function delMember(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {
			include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';
			if (strpos($_POST['delids'],',') !== false){
				$ids = explode(',', sanitize_text_field($_POST['delids']));
			}else{
				$ids[] = sanitize_user($_POST['delids']);
			}
			$uids = "'".rtrim(implode("','",$ids),",'")."'";
			EYOUNGCHAT_Roommember::del($uids);
			$ret['code'] = 1;
			self::retJson($ret);
		}
		$ret['code'] = 0;
		self::retJson($ret);
	}
	
	
	public static function setManager(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {
			if (!empty($_POST['key']) && ($_POST['key']=='manager') && (!empty($_POST['uid']))){
				include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';
				$val = (!empty($_POST['val'])) ? self::$role_ser : 0;
				$uid = sanitize_user($_POST['uid']);
				EYOUNGCHAT_Roommember::update(array('lvl'=>$val), array('uid'=>$uid));
				$ret['code'] = 1;
				self::retJson($ret);
			}
		}
		$ret['code'] = 0;
		self::retJson($ret);
	}
	
	public static function setStopSpeak(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {
			include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';
			global $wpdb;
			$pdata['uid'] = sanitize_user($_POST['uid']);
			$pdata['lvl'] = intval($_POST['lvl']);
			$pdata['stopspeak'] = (!empty($_POST['val'])) ? 1 : 0;
			$pdata['mtime'] = time();
			$wpdb->replace($wpdb->prefix.EYOUNGCHAT_Roommember::$tname, $pdata);
			$ret['code'] = 1;
			$ret['data'] = $pdata;
			self::retJson($ret);
		}
		$ret['code'] = 0;
		self::retJson($ret);
	}

	public static function setStopEnter(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {
			include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';
			$uid = sanitize_user($_POST['uid']);
			$lvl = intval($_POST['lvl']);
			$pdata['stopenter'] = (!empty($_POST['val'])) ? 1 : 0;
			$pdata['mtime'] = time();
			$row = EYOUNGCHAT_Roommember::getRow(array('uid'=>$uid));
			if (!empty($row)){
				if (($row->stopspeak == '0') && (empty($pdata['stopenter'])) ){
					EYOUNGCHAT_Roommember::del($uid);
				}else{
					$pdata['lvl'] = $lvl;
					EYOUNGCHAT_Roommember::update($pdata, array('uid'=>$uid));
				}
			}else{
				$pdata['uid'] = $uid;
				$pdata['lvl'] = $lvl;
				EYOUNGCHAT_Roommember::add($pdata);
			}
						
			$retdata = $pdata;
			$retdata['uid'] = $uid;
			$retdata['lvl'] = $lvl;
			$ret['code'] = 1;
			$ret['data'] = $retdata;
			self::retJson($ret);
		}
		$ret['code'] = 0;
		self::retJson($ret);
	}	
	
	
	public static function getChat(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {
			include_once EYOUNG_CHAT_PATH.'/table/chat_content.php';
			$limit = empty($_POST['limit']) ? 10 : intval($_POST['limit']);
			$resultempty = FALSE;
			$username =  $content = $extra = '';
			$page = max(1, intval($_POST['page']));
			$username = empty($_POST['username']) ? '' :sanitize_user($_POST['username']);
			$extra .= $username;
			$content = empty($_POST['content']) ? '' : sanitize_text_field($_POST['content']);
			$extra .= $content;
			$rid = self::$rid;
			$ret['data'] = EYOUNGCHAT_Content::get($rid, '*', $username,$content, ($page - 1) * $limit, $limit);
			$ret['count'] = EYOUNGCHAT_Content::count($rid,$username,$content);
			$ret['code'] = 1;
			self::retJson($ret);
		}
		$ret['code'] = 0;
		self::retJson($ret);
	}
	
	public static function delChat(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {
			if (!empty($_POST['delids'])){
				include_once EYOUNG_CHAT_PATH.'/table/chat_content.php';
				if (strpos($_POST['delids'],',') !== false){
					$ids = explode(',', sanitize_text_field($_POST['delids']));
				}else{
					$ids[] = intval($_POST['delids']);
				}
				$delids = "'".rtrim(implode("','",$ids),",'")."'";
				EYOUNGCHAT_Content::del(self::$rid,$delids);
				$ret['code'] = 1;
				self::retJson($ret);
			}
		}
		$ret['code'] = 0;
		self::retJson($ret);
	}
	
	public static function delChatAll(){
		if(!empty($_POST["formhash"]) && ($_POST["formhash"] == wp_create_nonce())) {
			include_once EYOUNG_CHAT_PATH.'/table/chat_content.php';
			EYOUNGCHAT_Content::del(self::$rid);
			$ret['code'] = 1;
			self::retJson($ret);
		}
		$ret['code'] = 0;
		self::retJson($ret);		
	}
		
	public static function setImageUpload(){
		$_t = wp_get_attachment_image_src(media_handle_upload( 'file', 0));
		$ret['code'] = 1;
		$ret['msg'] = $_t[0];
		self::retJson($ret);
	}
	
	public static function goChat(){
		$cfg = get_option(self::$optname);
		if (!empty($cfg['chatpageid'])){
			$centerurl = get_page_link($cfg['chatpageid']);
			echo '<script language="javascript">document.location= "'.$centerurl.'";</script>';
		}
	}
	
	public static function init(){
		if (is_admin()) {
			register_activation_hook(EYOUNG_CHAT_FILE, [__CLASS__, 'plugin_activate']);
			register_deactivation_hook(EYOUNG_CHAT_FILE, [__CLASS__, 'plugin_deactivate']);
			register_uninstall_hook(EYOUNG_CHAT_FILE, [__CLASS__, 'plugin_uninstall']);
			add_action('admin_menu', array(__CLASS__, 'admin_menu_handler'));
			add_action('wp_ajax_eychat_setting',array(__CLASS__,'manageSetting'));
			add_action('wp_ajax_eychat_upsocket',array(__CLASS__,'upSocketUrl'));
			add_action('wp_ajax_eychat_getMember',array(__CLASS__,'getMember'));
			add_action('wp_ajax_eychat_setMember',array(__CLASS__,'setMember'));
			add_action('wp_ajax_eychat_delMember',array(__CLASS__,'delMember'));
			add_action('wp_ajax_eychat_setManager',array(__CLASS__,'setManager'));
			add_action('wp_ajax_eychat_setStopSpeak',array(__CLASS__,'setStopSpeak'));
			add_action('wp_ajax_eychat_setStopEnter',array(__CLASS__,'setStopEnter'));
			add_action('wp_ajax_eychat_getChat',array(__CLASS__,'getChat'));
			add_action('wp_ajax_eychat_delChat',array(__CLASS__,'delChat'));
			add_action('wp_ajax_eychat_delChatAll',array(__CLASS__,'delChatAll'));
			add_filter('plugin_action_links', array(__CLASS__, 'setQuickUrl'), 10, 2);
			add_action('admin_enqueue_scripts', array(__CLASS__,'setLoadScripts'));
		}
		
		add_action('wp_ajax_eychat_imageupload',array(__CLASS__,'setImageUpload'));
	}
	
	public static function setLoadScripts(){
		wp_enqueue_style( 'eychat_eyui_admin_css', EYOUNG_CHAT_URL.EYOUNG_CHAT_JS.'/layui/css/layui.css');
		wp_enqueue_script('eychat_eyui_js', EYOUNG_CHAT_URL.EYOUNG_CHAT_JS.'/layui/layui.js');
	}
	
	public static function setQuickUrl($links, $file){
		if ($file != plugin_basename(EYOUNG_CHAT_FILE)) return $links;
		array_unshift($links, '<a href="'.self::$api_pay.'" target="_blank"><span style="color: blue;">延期&扩容</span></a>');
		array_unshift($links, '<a href="' . menu_page_url('ey_chat-setting', false) . '">设置</a>');
		return $links;
	}
		
	private static function getInitOpt(){
		$_opt['title'] = 'Ey聊天极简版';
		$_opt['notice'] = '这是演示公告';
		$_opt['limitmember'] = 10;
		$_opt['limitonline'] = 10;
		$_opt['membernumb'] = 0;
		$_opt['roomtype'] = 0;
		$_opt['notice'] = '';
		$_opt['status'] = 1;
		$_opt['sort'] = 0;
		$_opt['rid'] = 0;
		$_opt['isopen'] = 1;
		$_opt['faceopen'] = 1;
		$_opt['join']	= 0;
		$_opt['join2circle'] = 12;
		$_opt['allowtalk']	= 0;
		$_opt['savechat']	= 1;
		$_opt['chatshow']	= 1;
		$_opt['limitcontent'] = 100;
		$_opt['limitspeed'] = 0;
		$_opt['imageupload'] = 1;
		$_opt['imagelimit'] = 1024;
		$_opt['imagenotvisitor'] = 1;
		$_opt['fileupload'] = 1;
		$_opt['filelimit'] = 2048;
		$_opt['filenotvisitor'] = 1;
		$_opt['fileext'] = 'zip|rar|7z|doc|docx|pdf';
		$_opt['maskwords'] = '';
		return $_opt;	
	}
	
	private static function setInstallSql(){
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');	
		global $wpdb;
		$prefix = $wpdb->prefix.self::$name;
		$charset_collate = $wpdb->get_charset_collate();
		$sql = "CREATE TABLE `{$prefix}_room_member`  (
			  `pid` int(11) NOT NULL AUTO_INCREMENT,
			  `rid` int(11) DEFAULT '0',  
			  `uid` varchar(20) DEFAULT NULL,
			  `lvl` tinyint(4) DEFAULT NULL,
			  `stopspeak` tinyint(4) DEFAULT '0',
			  `stopenter` tinyint(4) DEFAULT '0',    
			  `status` tinyint(4) DEFAULT '1',  
			  `mtime` bigint(20) DEFAULT NULL,
			  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
			  PRIMARY KEY (`pid`),
			  UNIQUE INDEX `uid`(`uid`) USING BTREE
			) {$charset_collate};";
		dbDelta($sql);
		$sql = "CREATE TABLE `{$prefix}_room_apply`  (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `rid` int(11) DEFAULT '0',  
			  `uid` varchar(20) DEFAULT NULL,
			  `lvl` tinyint(4) DEFAULT NULL,  
			  `name` varchar(30) DEFAULT NULL,  
			  `apply` varchar(200) DEFAULT NULL,
			  `status` tinyint(4) DEFAULT '0',
			  `mtime` bigint(20) DEFAULT NULL,
			  `mid` bigint(20) DEFAULT NULL,   
			  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
			  PRIMARY KEY (`id`),
			  INDEX `rid`(`rid`) USING BTREE,
			  INDEX `uid`(`uid`) USING BTREE,  
			  INDEX `ruid`(`rid`, `uid`) USING BTREE  
			) {$charset_collate};";
		dbDelta($sql);
		$sql = "CREATE TABLE `{$prefix}_content` (
			  `cid` int(11) NOT NULL AUTO_INCREMENT,
			  `rid` int(11) DEFAULT '0',
			  `content` mediumtext,
			  `uid` varchar(32) DEFAULT NULL,
			  `name` varchar(32) DEFAULT NULL,
			  `lvl` tinyint(4) DEFAULT NULL,  
			  `oid` varchar(32) DEFAULT NULL,
			  `oname` varchar(32) DEFAULT NULL,
			  `status` tinyint(4) DEFAULT '1',
			  `busflag` int(11) DEFAULT '0',
			  `ctime` bigint(20) DEFAULT NULL,
			  PRIMARY KEY (`cid`),
			  KEY `uid` (`uid`),
			  KEY `oname` (`oname`),
			  INDEX `rid`(`rid`) USING BTREE,  
			  INDEX `cid`(`cid`, `rid`) USING BTREE  
			) {$charset_collate};";
		dbDelta($sql);
	}
	
	public static function plugin_activate(){
	
		$__opt = get_option(self::$optname);
		$_opt = empty($__opt) ? self::getInitOpt() : $__opt;
		
		$_opt['chatpageid'] = self::setChatPage($_opt['title'], 'ey_chat', 'ey_chatpage.php', '[ey_chatpage]');
		if (empty($__opt))
			add_option(self::$optname, $_opt);
		else
			update_option(self::$optname, $_opt);
		self::setInstallSql();

		$_uinfo = wp_get_current_user();
		if (!empty($_uinfo->ID)){
			include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';		
			EYOUNGCHAT_Roommember::add(array('rid'=>self::$rid,'uid'=>$_uinfo->ID,'lvl'=>self::$role_ser));
		}
	
		$response = wp_remote_post(self::$api_reg.'/plugin/chat/install',array('method' => 'POST','sslverify' => false,'body' => array('weburl' => site_url(),'webkey' => AUTH_KEY,'k' => AUTH_SALT,'v' => md5(AUTH_SALT.AUTH_KEY.site_url()))));
		if (is_wp_error( $response ) ) {
			$ret['code'] = 0;
			$ret['msg'] = $response->get_error_message();
			self::retJson($ret);
		} else {
			$_res = wp_remote_retrieve_body($response);
			$data = json_decode($_res, true);
			if ($data['code'] == '1'){
				foreach ($data['msg'] as $k => $v) $_opt[$k] = $v;
				update_option(self::$optname, $_opt);
			}
		}
	}
	
	private static function setChatPage($title, $slug, $page_template='', $post_content=''){
		$allPages = get_pages();//获取所有页面
		$exists = false;
		foreach( $allPages as $page ){
			//通过页面别名来判断页面是否已经存在
			if( strtolower( $page->post_name ) == strtolower( $slug ) ){
				return $page->ID;
				$exists = true;
			}
		}
		if( $exists == false ) {
			$new_page_id = wp_insert_post(
				array(
					'post_title'	=> $title,
					'post_type'	=> 'page',
					'post_name'	=> $slug,
					'comment_status'=> 'closed',
					'ping_status'	=> 'closed',
					'post_content'	=> $post_content,
					'post_status'	=> 'publish',
					'post_author'	=> 1,
					'menu_order'	=> 0
				)
			);
			//如果插入成功 且设置了模板   
			if($new_page_id && $page_template!=''){
				//保存页面模板信息
				update_post_meta($new_page_id, '_wp_page_template',  $page_template);
			}
			return $new_page_id;
		}
	}

	public static function plugin_deactivate(){
		delete_option(self::$optname);
		global $wpdb;
		$prefix = $wpdb->prefix.self::$name;
		$wpdb->query("DROP TABLE IF EXISTS `{$prefix}_content`;");
		$wpdb->query("DROP TABLE IF EXISTS `{$prefix}_room_member`;");
		$wpdb->query("DROP TABLE IF EXISTS `{$prefix}_room_apply`;");
		$wpdb->query("DROP TABLE IF EXISTS `{$prefix}_attach`;");	
	}

	public static function plugin_uninstall(){
		delete_option(self::$optname);
		global $wpdb;
		$prefix = $wpdb->prefix.self::$name;
		$wpdb->query("DROP TABLE IF EXISTS `{$prefix}_content`;");
		$wpdb->query("DROP TABLE IF EXISTS `{$prefix}_room_member`;");
		$wpdb->query("DROP TABLE IF EXISTS `{$prefix}_room_apply`;");
		$wpdb->query("DROP TABLE IF EXISTS `{$prefix}_attach`;");
	
		$response = wp_remote_post(self::$api_reg.'/plugin/chat/uninstall',array('method' => 'POST','sslverify' => false,'body' => array('weburl' => site_url(),'webkey' => AUTH_KEY,'k' => AUTH_SALT,'v' => md5(AUTH_SALT.AUTH_KEY.site_url()))));
		if (is_wp_error( $response ) ) {
			$ret['code'] = 0;
			$ret['msg'] = $response->get_error_message();
			self::retJson($ret);
		}
	}

	public static function admin_menu_handler(){
		global $submenu;
		add_menu_page(
			'Ey聊天极简版',
			'Ey聊天极简版',
			'administrator',
			self::$name, 
			array(__CLASS__, 'manageSetting'),
			'dashicons-format-chat'
		);
		add_submenu_page(self::$name,'Ey聊天极简版','设置', 'administrator', self::$name.'-setting', array(__CLASS__,'manageSetting'));
		add_submenu_page(self::$name,'Ey聊天极简版','访问', 'administrator', self::$name.'-gochat', array(__CLASS__,'goChat'));
		unset($submenu[self::$name][0]);
	}
}
