<?php
class EYOUNGCHAT_Front{

	public static $lg = array();
	public static $t = null;
	
	public static  $uinfo = array();
	
	public function __construct(){
	}
	
	private static function getUserInfo(){
		if (!empty(self::$uinfo)) return self::$uinfo;
		$_uinfo = wp_get_current_user();
		if (empty($_uinfo->ID)){
			$_member['uid'] = 'v'.substr(md5($_SERVER["REMOTE_ADDR"].$_SERVER["HTTP_USER_AGENT"]),10,5);
			$_member['name'] = self::$lg['visitor'].$_member['uid'];
			$_member['gid'] = '';
			$_member['lvl'] = EYOUNGCHAT_Admin::$role_ver;
		}else{
			$_member['uid'] = $_uinfo->ID;
			$_member['name'] = $_uinfo->display_name;
			$_member['gid'] = $_uinfo->roles[0];
			$_member['lvl'] = EYOUNGCHAT_Admin::$role_uer;
		}
		self::$uinfo = $_member;
		return $_member;
	}
	
	private static function getUserAvatar($num='') {
//		if (empty($avatar_html)) return EYOUNG_CHAT_URL.'assets/images/ver.png';
//		preg_match('/src=["|\'](.+)[\&|"|\']/U', $avatar_html, $matches);
//		if ( isset( $matches[1] ) && ! empty( $matches[1] ) ) {
//			return esc_url_raw($matches[1]);
//		}
		return EYOUNG_CHAT_URL.'images/'.(empty($num) ? rand(1,40) : intval($num)).'.png';
	}
	
	private static function setUinfoSign($uinfo, $hash){
		return md5($uinfo['uid'].$uinfo['lvl'].$hash);
	}
	
	private static function checkUinfoSign($uinfo, $hash){
		if (empty($uinfo['v'])) return false;
		return ($uinfo['v'] == self::setUinfoSign($uinfo, $hash)) ? true : false;
	}
		
	private static function checkService($severinfo, $uid){	
		return in_array($uid, array_keys($severinfo)) ? true : false;
	}

	private static function checkValidUid($uid){
		if (!is_numeric($uid) && (strlen($uid) != 6)){
			return false;				
		}else if(!is_numeric($uid) && (strlen($uid) == 6) && (substr($uid,0,1) == 'v')) {
			preg_match_all('/[a-zA-Z0-9]{6}/u',$uid, $s);
			return $s[0][0];
		}else if(is_numeric($uid) && (strlen($uid) != 6)){
			return intval($uid);
		}
	}

	private static function checkValidRid($ary){
		asort($ary, 2);
		return implode('_', $ary);
	}
	
	private static function getUserFilter($pinfo){
		$ret['uid'] = self::checkValidUid($pinfo['uid']);
		$ret['name'] = sanitize_user($pinfo['name']);
		$ret['lvl'] = intval($pinfo['lvl']);
		if (!empty($pinfo['v'])) $ret['v'] = filter_var($pinfo['v'], FILTER_SANITIZE_STRING);
		return $ret;
	}
	
	public static function setAjx(){
		if (!self::checkUinfoSign($_POST['uinfo'], $_POST['formhash'])){
			$ret['code'] = 0;
			$ret['msg'] = self::$lg['tip_reterr'];
			EYOUNGCHAT_Admin::retJson($ret);
		}
		
		switch ($_POST['action']) {
			case 'eychat_getHistory':
				self::getHistory();
				break;
			case 'eychat_setNotice':
				self::setNotice();
				break;
			case 'eychat_setContent':
				self::setContent();
				break;
			case 'eychat_getStatus':
				self::getStatus();
				break;
			case 'eychat_setUserSpeak':
				self::setUserSpeak();
				break;
			case 'eychat_setUserEnter':
				self::setUserEnter();
				break;
			case 'eychat_checkJoin2':
				self::checkJoin2();
				break;
			case 'eychat_checkJoin3':
				self::checkJoin3();
				break;
			case 'eychat_getApply':
				self::getApply();
				break;
			case 'eychat_setApplyPass':
				self::setApplyPass();
				break;
			case 'eychat_setApplyReject':
				self::setApplyReject();
				break;
		}
	}
	
	private static function setContent(){

		$uinfo = self::getUserFilter($_POST['uinfo']);
	
		$adata['content'] = substr(sanitize_textarea_field(trim($_POST['content'])), 0, 1000);	
		
		if (empty($adata['content'])){
			$ret['code'] = 0;
			$ret['msg'] = self::$lg['tip_reterr'];
			EYOUNGCHAT_Admin::retJson($ret);
		}
		
		include_once EYOUNG_CHAT_PATH.'/table/chat_content.php';


		$setting = get_option(EYOUNGCHAT_Admin::$optname);
	
		if ((($setting['allowtalk'] == '1') && (in_array($uinfo['gid'], $setting['allowtalk1']) == false)) || ($setting['allowtalk'] == '2')) {
			$retdata['code'] = 0;
			$retdata['msg'] = self::$lg['tip_stopspeak'];
			EYOUNGCHAT_Admin::retJson($ret);
		}

		if (strpos($setting['maskwords'],',') !== false){
			$_patterns = explode(",", $setting['maskwords']);
			foreach ($_patterns as $k => $v) if (!empty($v)) $patterns[$k] = '/' . $v . '/i';
			$replaces = array_fill(0, count($patterns), '*****');
			$adata['content'] = preg_replace($patterns, $replaces, $adata['content']);
		}
	
		$adata['rid'] = 0;
		$adata['uid'] = $uinfo['uid'];
		$adata['lvl'] = $uinfo['lvl'];
		$adata['name'] = $uinfo['name'];
		$adata['content'] = wp_unslash($adata['content']);
		$adata['ctime'] = time();		
		if (!empty($_POST['atinfo'])) {
			$adata['oid'] = filter_var($_POST['atinfo'][0]['uid'], FILTER_SANITIZE_STRING);
			$adata['oname'] = filter_var($_POST['atinfo'][0]['uid'], FILTER_SANITIZE_STRING);
			foreach ($_POST['atinfo'] as $k => $v){
				if(empty($v['offline'])){
					$onlinetip[] = filter_var($v['uid'], FILTER_SANITIZE_STRING);
				}
			}
		}
		
		if ($setting['savechat']){
			$adata['cid'] = EYOUNGCHAT_Content::add($adata);
		}else{
			$adata['cid'] = uniqid();
		}

		if (!empty($onlinetip)) $adata['atinfo'] = $onlinetip;
		$ret['code'] = 1;
		$ret['data'] = $adata;
		EYOUNGCHAT_Admin::retJson($ret);
	}
	
	private static function getHistory(){
		include_once EYOUNG_CHAT_PATH.'/table/chat_content.php';
	
		$cid = empty($_POST['cid']) ? '' : intval($_POST['cid']);
		$ret['code'] = 1;
		$ret['data'] = EYOUNGCHAT_Content::getChat(0,$cid, 10);
		EYOUNGCHAT_Admin::retJson($ret);
	}

	private static function setNotice(){

		$uinfo = self::getUserFilter($_POST['uinfo']);

		if ($uinfo['lvl'] != EYOUNGCHAT_Admin::$role_ser){
			$ret['code'] = 0;
			$ret['msg'] = self::$lg['tip_nopower'];
			EYOUNGCHAT_Admin::retJson($ret);
		}

		$notice = wp_unslash(sanitize_text_field($_POST['notice']));
		$_opt = get_option(EYOUNGCHAT_Admin::$optname);
		$_opt['notice'] = $notice;
		update_option(EYOUNGCHAT_Admin::$optname, $_opt);
		$pdata['rid'] = intval($_POST['rid']);
		$pdata['notice'] = nl2br($notice);
		$ret['code'] = 1;
		$ret['data'] = $pdata;
		EYOUNGCHAT_Admin::retJson($ret);
	}
	
	private static function getStatus(){

		$uinfo = self::getUserFilter($_POST['uinfo']);
		if ($uinfo['lvl'] != EYOUNGCHAT_Admin::$role_ser){
			$ret['code'] = 0;
			$ret['msg'] = self::$lg['tip_nopower'];
			EYOUNGCHAT_Admin::retJson($ret);
		}
	
		include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';
		$limit = empty($_POST['limit']) ? 10 : intval($_POST['limit']);
		$page = max(1, intval($_POST['page']));
		$sdata['rid'] = 0;
		$data = EYOUNGCHAT_Roommember::get($sdata,'uid,lvl,stopspeak,stopenter,mtime', ($page - 1) * $limit, 10);
		
		if (!empty($data)){
			$ids = array_column($data,'uid');
			$_u = get_users( array('include'=>$ids, 'fields' => array( 'id','display_name' ) ) );
			$ret['member'] = array_column($_u,'display_name','id');
		}
		
		$ret['count'] = empty($_POST['count']) ? EYOUNGCHAT_Roommember::count($sdata) : intval($_POST['count']);
		$ret['code'] = 1;
		$ret['data'] = $data;
		EYOUNGCHAT_Admin::retJson($ret);		
	}
	
	private static function setUserSpeak(){
		$uinfo = self::getUserFilter($_POST['uinfo']);
		if ($uinfo['lvl'] != EYOUNGCHAT_Admin::$role_ser){
			$ret['code'] = 0;
			$ret['msg'] = self::$lg['tip_nopower'];
			EYOUNGCHAT_Admin::retJson($ret);
		}
			
		include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';	
		$uid = self::checkValidUid($_POST['uid']);
		$pdata['stopspeak'] = (!empty($_POST['val'])) ? 1 : 0;
		$pdata['mtime'] = time();
		$rid = intval($_POST['rid']);
		$lvl = intval($_POST['lvl']);		
		
			$row = EYOUNGCHAT_Roommember::getRow(array('rid'=>$rid,'uid'=>$uid));
			if (!empty($row)){
				if (($row->stopenter == '0') && (empty($pdata['stopspeak'])) ){
					EYOUNGCHAT_Roommember::del($uid);
				}else{
					$pdata['lvl'] = $lvl;
					EYOUNGCHAT_Roommember::update($pdata, "rid=".$rid." and uid='".$uid."'");
				}
			}else{
				$pdata['rid'] = $rid;
				$pdata['uid'] = $uid;
				$pdata['lvl'] = $lvl;
				EYOUNGCHAT_Roommember::add($pdata);
			}
		
		$retdata = $pdata;
		$retdata['rid'] = $rid;
		$retdata['uid'] = $uid;
		$retdata['lvl'] = $lvl;
		$ret['code'] = 1;
		$ret['data'] = $retdata;
		EYOUNGCHAT_Admin::retJson($ret);	
	}
	
	private static function setUserEnter(){
		$uinfo = self::getUserFilter($_POST['uinfo']);
		if ($uinfo['lvl'] != EYOUNGCHAT_Admin::$role_ser){
			$ret['code'] = 0;
			$ret['msg'] = self::$lg['tip_nopower'];
			EYOUNGCHAT_Admin::retJson($ret);
		}
			
		include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';	
		$uid = self::checkValidUid($_POST['uid']);
		$pdata['stopenter'] = (!empty($_POST['val'])) ? 1 : 0;
		$pdata['mtime'] = time();
		$rid = intval($_POST['rid']);
		$lvl = intval($_POST['lvl']);	
		
		$row = EYOUNGCHAT_Roommember::getRow(array('rid'=>$rid,'uid'=>$uid));
		if (!empty($row)){
			if (($row->stopspeak == '0') && (empty($pdata['stopenter'])) ){
				EYOUNGCHAT_Roommember::del($uid);
			}else{
				$pdata['lvl'] = $lvl;
				EYOUNGCHAT_Roommember::update($pdata, array("rid"=>$rid,"uid"=>$uid));
			}
		}else{
			$pdata['rid'] = $rid;
			$pdata['uid'] = $uid;
			$pdata['lvl'] = $lvl;
			EYOUNGCHAT_Roommember::add($pdata);
		}
	
		$retdata = $pdata;
		$retdata['rid'] = $rid;
		$retdata['uid'] = $uid;
		$retdata['lvl'] = $lvl;
		$ret['code'] = 1;
		$ret['data'] = $retdata;	
		EYOUNGCHAT_Admin::retJson($ret);	

	}
	
	private static function checkJoin2(){
		$setting = get_option(EYOUNGCHAT_Admin::$optname);
		if ($_POST['passwd'] != $setting['join2']){
			$ret['code'] = 0;
			$ret['msg'] = self::$lg['tip_fail_join2'];
			EYOUNGCHAT_Admin::retJson($ret);
		}
		
		$rid = intval($_POST['rid']);
		setcookie('join2_'.$rid, $setting['join2'], current_time('timestamp')+$setting['join2circle']*3600, COOKIEPATH, COOKIE_DOMAIN, false);
		$ret['code'] = 1;
		EYOUNGCHAT_Admin::retJson($ret);
	}
	private static function checkJoin3(){
		$ret['code'] = 0;	
		if (trim($_POST['apply']) == ''){
			$ret['msg'] = self::$lg['tip_content'];
			EYOUNGCHAT_Admin::retJson($ret);
		}
		if (!isset($_POST['rid'])){
			$ret['msg'] = self::$lg['tip_fail_join3'];
			EYOUNGCHAT_Admin::retJson($ret);
		}
		$uinfo = self::getUserFilter($_POST['uinfo']);
		include_once EYOUNG_CHAT_PATH.'/table/chat_room_apply.php';
		$sdata['rid'] = intval($_POST['rid']);
		$sdata['uid'] = $uinfo['uid'];
		$sdata['status'] = 0;

		$row = EYOUNGCHAT_Roomapply::getRow($sdata);
		
		if (!empty($row)){
			$apply = wp_unslash(sanitize_text_field($_POST['apply']));
			EYOUNGCHAT_Roomapply::update(array('apply'=>$apply), "status=0 and rid={$sdata['rid']} and uid='{$sdata['uid']}'");
			
		}else{
			$sdata['apply'] = wp_unslash(sanitize_text_field($_POST['apply']));
			$sdata['name'] = $uinfo['name'];
			$sdata['lvl'] = $uinfo['lvl'];
			EYOUNGCHAT_Roomapply::add($sdata);
		}
		
		$ret['code'] =1;
		$ret['msg'] = self::$lg['tip_success_join3'];
		EYOUNGCHAT_Admin::retJson($ret);
	}
	
	
	private static function getApply(){
		$uinfo = self::getUserFilter($_POST['uinfo']);
		if ($uinfo['lvl'] != EYOUNGCHAT_Admin::$role_ser){
			$ret['code'] = 0;
			$ret['msg'] = self::$lg['tip_nopower'];
			EYOUNGCHAT_Admin::retJson($ret);
		}
			
		include_once EYOUNG_CHAT_PATH.'/table/chat_room_apply.php';
		$limit = empty($_POST['limit']) ? 10 : intval($_POST['limit']);
		$page = max(1, intval($_POST['page']));
		$sdata['rid'] = intval($_POST['rid']);
		$sdata['status'] = 0;
		$count = empty($_POST['count']) ? EYOUNGCHAT_Roomapply::count($sdata) : intval($_POST['count']);
		$ret['data'] = EYOUNGCHAT_Roomapply::get($sdata, "*", ($page - 1) * $limit, $limit);
		$ret['code'] = 1;
		$ret['count'] = $count;
		EYOUNGCHAT_Admin::retJson($ret);
	}

	private static function setApplyPass(){
		$uinfo = self::getUserFilter($_POST['uinfo']);
		$rid = intval($_POST['rid']);
		$roomtype = (!empty($_POST['roomtype'])) ? 1 : 0;
		if (!empty($_POST['ids'])){
			include_once EYOUNG_CHAT_PATH.'/table/chat_room_apply.php';
			foreach ($_POST['ids'] as $k => $v){}
			$ids = filter_var_array($_POST['ids'],FILTER_SANITIZE_NUMBER_INT);
			$_c = "rid IN ({$rid}) AND status=0 AND id in(". implode(',',$ids).")";
			$rs = EYOUNGCHAT_Roomapply::getRs($_c);
			if (!empty($rs)){
				include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';
				foreach ($rs as $k => $v){
					$adata['rid'] = $v->rid;
					$adata['uid'] = $v->uid;
					$adata['lvl'] = $v->lvl;
					if ($adata['uid'] == $uinfo['uid']){
						EYOUNGCHAT_Roomapply::del(array('rid'=>$rid,'id'=>$v->id,'status'=>0));
						continue;
					}
					EYOUNGCHAT_Roommember::add($adata, false, true);
					EYOUNGCHAT_Roomapply::update(array('status'=>1,'mid'=>$uinfo['uid'],'mtime'=>time()),array('rid'=>$rid,'id'=>$v->id));
				}
			}else{
				$ret['code'] = 0;
				EYOUNGCHAT_Admin::retJson($ret);
			}
			$ret['code'] = 1;
			EYOUNGCHAT_Admin::retJson($ret);
		}

		$ret['code'] = 0;
		EYOUNGCHAT_Admin::retJson($ret);
	}
	
	private static function setApplyReject(){
		$uinfo = self::getUserFilter($_POST['uinfo']);
		$rid = intval($_POST['rid']);

		if (!empty($_POST['ids'])){
			$ids = sanitize_text_field($_POST['ids']);
			include_once EYOUNG_CHAT_PATH.'/table/chat_room_apply.php';

			$_c = '';
			foreach ($ids as $k => $v) $_c .= " (status=0 and rid={$rid} and id={$v}) or";
			EYOUNGCHAT_Roomapply::del(rtrim($_c,'or'));
			$ret['code'] = 1;
			EYOUNGCHAT_Admin::retJson($ret);
		}
		
		$ret['code'] = 0;
		EYOUNGCHAT_Admin::retJson($ret);
	}

	public static function uploadImage(){
		$uinfo = self::getUserFilter($_POST['uinfo']);
		$_src = wp_get_attachment_metadata(media_handle_upload('file',0));
		if (!empty($_src['file'])){
			$wp_dir = wp_upload_dir();
			$storepath = $wp_dir['url'].'/';
			$ret['thumb'] = (!empty($_src['sizes']['thumbnail']['file'])) ? ($storepath.$_src['sizes']['thumbnail']['file']) : '';
			$ret['large'] = (!empty($_src['sizes']['large']['file'])) ? ($storepath.$_src['sizes']['large']['file']) : ((!empty($_src['sizes']['medium']['file'])) ? ($storepath.$_src['sizes']['medium']['file']) : '');
			$ret['thumb'] = $ret['thumb'] ? $ret['thumb'] : $wp_dir['baseurl'] .'/'. $_src['file'];
			$ret['large'] = $ret['large'] ? $ret['large'] : $wp_dir['baseurl'] .'/' . $_src['file'];
			$ret['code'] = 1;
			EYOUNGCHAT_Admin::retJson($ret);
		}

		$ret['code'] = 0;
		EYOUNGCHAT_Admin::retJson($ret);
	}
	public static function uploadFile(){
		$uinfo = self::getUserFilter($_POST['uinfo']);
		$ret['code'] = 1;
		$ret['url'] = wp_get_attachment_url(media_handle_upload('file',''));
		EYOUNGCHAT_Admin::retJson($ret);
	}
 
 	public static function setJoin2Cookie($rid=0, $val=''){
		$setting = get_option(EYOUNGCHAT_Admin::$optname);
		setcookie('join2_'.$rid, time(), current_time('timestamp')+$setting['join2circle']*3600, COOKIEPATH, COOKIE_DOMAIN, false);
 	}
 
	public static function init(){		
		$cfg = get_option(EYOUNGCHAT_Admin::$optname);
		if (wp_is_mobile()){
			if (empty($cfg['mobileopen'])) return;
		}else{
			if (empty($cfg['isopen'])) return;
		}
		include_once EYOUNG_CHAT_PATH.'/lang/front.php';
		self::$lg = $lang;
		
		add_action('plugins_loaded', array(__CLASS__, 'renderScript'));
		add_action('init',  array(__CLASS__,'setJoin2Cookie'));
	}
	
	public static function renderScript(){
	
		$uinfo = self::getUserInfo();
		add_action('wp_ajax_eychat_setContent', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_getHistory', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_checkJoin2', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_checkJoin3', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_getApply', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_setApplyPass', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_setApplyReject', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_setNotice', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_getStatus', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_setUserSpeak', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_setUserEnter', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_eychat_uploadImage', array(__CLASS__, 'uploadImage'));
		add_action('wp_ajax_eychat_uploadFile', array(__CLASS__, 'uploadFile'));

		add_action('wp_ajax_nopriv_eychat_setContent', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_nopriv_eychat_getHistory', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_nopriv_eychat_checkJoin2', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_nopriv_eychat_checkJoin3', array(__CLASS__, 'setAjx'));
		add_action('wp_ajax_nopriv_eychat_uploadImage', array(__CLASS__, 'uploadImage'));
		add_action('wp_ajax_nopriv_eychat_uploadFile', array(__CLASS__, 'uploadFile'));
		
		add_filter('template_include',array(__CLASS__, 'getChatPage'),11);
		add_action('wp_enqueue_scripts', array(__CLASS__,'setLoadScripts'));
	}
	

	public static function setLoadScripts(){
		wp_enqueue_style( 'eychat_layui_css', EYOUNG_CHAT_URL.EYOUNG_CHAT_JS.'/layui/css/layui.css',12);
		wp_enqueue_style( 'eychat_def_css', EYOUNG_CHAT_URL.EYOUNG_CHAT_JS.'/layui/css/eydef.css',13);
		wp_enqueue_script('eychat_js', EYOUNG_CHAT_URL.EYOUNG_CHAT_JS.'/layui/layui.js');			
	}

	public static function getChatPage( $template ) {
		if (is_page('ey_chat')){
			include_once EYOUNG_CHAT_PATH.'/table/chat_room_member.php';
			include_once EYOUNG_CHAT_PATH.'/table/chat_content.php';
			$uinfo = self::getUserInfo();
			$rid = EYOUNGCHAT_Admin::$rid;
			$sdata['rid'] = $rid;
			$sdata['status'] = 1;
			$setting = get_option(EYOUNGCHAT_Admin::$optname);
	
			$sdata['uid'] = $uinfo['uid'];
			$user = EYOUNGCHAT_Roommember::getRow($sdata,'lvl,stopspeak,stopenter');
			$uinfo['lvl'] = ($user) ? ($user->lvl) : $uinfo['lvl'];
			
			if ($user->stopspeak) $uinfo['stopspeak'] = 1;
			if ($user->stopenter) $uinfo['stopenter'] = 1;

			$uinfo['st'] = 0;
			if ($setting['join'] == "1") {
				$uinfo['st'] = in_array($uinfo['gid'], $setting['join1']) ? 0 : 1;
			}else if ($setting['join'] == "2"){
				$_join = filter_var($_COOKIE["join2_".$rid], FILTER_SANITIZE_STRING);
				$uinfo['st'] = ($_join!= $setting['join2']) ? 2 : 0;
			}else if ($setting['join'] == "3"){
				$uinfo['st'] = ($user) ? ($user->stopenter ? 4 : 0) : 3;
			}
	
			$uinfo['st'] = (!empty($user->stopenter)) ? 4 : $uinfo['st'];

			if ((($setting['allowtalk'] == '1') && (in_array($uinfo['gid'], $setting['allowtalk1']) == false)) || ($setting['allowtalk'] == '2')) {
				$uinfo['stopspeak'] = 1;
			}
	
			$uinfo['v'] = self::setUinfoSign($uinfo, wp_create_nonce());

			unset($setting['join2'],$setting['maskwords']);
			

			if (!empty($setting['chatshow'])) $chat = EYOUNGCHAT_Content::getChat($rid,'', 10);

			$check = md5($setting['license'].$uinfo['uid']);
			$lang = self::$lg;
						
			if ($uinfo['st'] == 4){
				return  EYOUNG_CHAT_PATH . '/assets/tip.php';
			}else if ($uinfo['st'] == 1){
				return  EYOUNG_CHAT_PATH . '/assets/tip.php';
			}

		 	include_once EYOUNG_CHAT_PATH . '/assets/chat.php';
			return false;
		}
		return $template;
	}	
}
