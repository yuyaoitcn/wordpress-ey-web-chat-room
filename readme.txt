=== Eyoung Chat - Ey聊天极简版 ===
Contributors: eyoung
Donate link: https://service.yuyaoit.cn/wpplugin/index?app=ey-chat
Tags: 群聊,聊天,沟通,互动,WebIM,Chat
Requires at least: 4.8
Tested up to: 5.9
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Eyoung Chat System (Ey聊天极简版), 为WordPress网站提供网页版的在线即时沟通工具.

== Description ==

一个网页版本的聊天室，在线成员可以群聊，可以相互@发言，支持PC浏览器和手机浏览器自适应；
可以设置聊天记录的保存，推流技术使用websocket协议，响应速度快，支持迸发高。

== Installation ==

### 1.安装

方式1：上传安装

FTP上传安装
1. 解压插件压缩包`eyoung-chat.zip`，将解压获得文件夹上传至wordpress安装目录下的 `/wp-content/plugins/` 目录.
2. 访问WordPress仪表盘，进入"插件"-"已安装插件"，在插件列表中找到"Ey聊天极简版"插件，点击"启用".
3. 通过`Ey聊天极简版`->`插件设置` 进行插件设置。

仪表盘上传安装
1. 进入WordPress仪表盘，点击`插件-安装插件`；
2. 点击界面左上方的`上传按钮`，选择本地提前下载好的插件压缩包`eyoung-chat.zip`，点击`现在安装`；
3. 安装完毕后，启用`Eyoung在线客服系统`插件；
4. 通过`Ey聊天极简版`->`插件设置` 进行插件设置。

方式2：上传安装

FTP上传安装
1. 解压插件压缩包，将解压获得文件夹上传至wordpress安装目录下的 `/wp-content/plugins/`目录.
2. 访问WordPress仪表盘，进入"插件"-"已安装插件"，在插件列表中找到"Ey聊天极简版"，点击"启用".
3. 通过"设置"->"Ey聊天极简版" 进入插件设置界面.

仪表盘上传安装
1. 进入WordPress仪表盘，点击"插件-安装插件"；
2. 点击界面左上方的"上传按钮"，选择本地提前下载好的插件压缩包点击"现在安装"；
3. 安装完毕后，启用 `Ey聊天极简版` 插件；
4. 通过"设置"->"Ey聊天极简版" 进入插件设置界面.


== Frequently Asked Questions ==

= 为何发送消息后反应有些滞后？ =
可能是wordpress的空间服务器记录聊天内容时的延时导致，可以在插件后台设置处把『记录聊天内容』选项关闭，然后保存后再试即可。



== Screenshots ==

1. Ey聊天极简版后台设置截图。
2. Ey聊天极简版前端用户使用截图。

== Changelog ==

= 1.0 =2022/02/18
* 新发布

== Upgrade Notice ==
