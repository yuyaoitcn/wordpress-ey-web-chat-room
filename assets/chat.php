<?php  get_header();?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title><?php _e($cfg['title']);?></title>
</head>
<body>
  <div class="layui-container">  
	<div class="layui-row layui-col-space15" id="eychat">
		<div class="layui-col-xs1"></div>	
		<div class="layui-col-xs10">
			<div class="layui-tab layui-tab-card"  lay-filter="roomTab" id="roomTab">
				<ul class="layui-tab-title">
				</ul>
				<div class="layui-tab-content" style="padding-top:10px;">
				</div>
			</div>
		</div>	
		<div class="layui-col-xs1"></div>
	</div>
</div>
<script type="text/html" id="uinfo-st1-tpl">
 <div class="layui-row layui-col-space5">
  <div class="layui-col-xs2">
  </div>
  <div class="layui-col-xs8 layui-bg-gray" style="margin:50px 0 50px 0;">
   <div class="layui-panel">
	<div style="padding: 50px 30px;" class="layui-font-20">
	<i class="layui-icon layui-icon-about" style="font-size:22px;"></i> 您所属的用户组无法访问
	</div>
   </div>
  </div>
  <div class="layui-col-xs2">
  </div>
 </div>
</script>
<script type="text/html" id="uinfo-st4-tpl">
 <div class="layui-row layui-col-space5">
  <div class="layui-col-xs2">
  </div>
  <div class="layui-col-xs8 layui-bg-gray" style="margin:50px 0 50px 0;">
   <div class="layui-panel">
	<div style="padding: 50px 30px;" class="layui-font-20">
	<i class="layui-icon layui-icon-about" style="font-size:22px;"></i> 您被禁入，请与管理员联系。
	</div>
   </div>
  </div>
  <div class="layui-col-xs2">
  </div>
 </div>
</script>
<script type="text/html" id="uinfo-st2-tpl">
<div class="layui-row layui-col-space5">
	<div class="layui-col-xs3"></div>	
	<div class="layui-col-xs6 layui-bg-gray" style="margin:50px 0 50px 0;">
		<div class="layui-card">
		  <div class="layui-card-header">{{d.title}}</div>
		  <div class="layui-card-body">
			  <div class="layui-row layui-col-space15">
				  <div class="layui-col-xs4">
					{{#  if(d.avatar != ''){ }}
					<img class="group-avatar" src="<?php _e(EYOUNG_CHAT_URL); ?>/images/group_logo.png">
					{{#  } else { }}
					<img class="group-avatar" src="<?php _e(EYOUNG_CHAT_URL); ?>/images/group_logo.png">
					{{#  } }}  
				  </div>
				  <div class="layui-col-xs8 list-intro">
					<form class="layui-form">
						<input type="hidden" name="rid" value="{{ d.rid }}">
						<div class="fox-form">
							<div class="layui-form-item">
									<input class="layui-input" name="passwd" lay-verify="required" autocomplete="off" placeholder="请输入入群口令" type="password">
							</div>
							<div class="form-group">
								<div class="field">
									<button class="layui-btn layui-btn-primary"  style="width:100%;" lay-submit lay-filter="submitJoin2"><i class="layui-icon layui-icon-ok"></i> 确 定</button>
								</div>
							</div>
						</div>
					</form>
				  </div>
			  </div>
		  </div>
		</div>
	</div>
</div>
</script>
<script type="text/html" id="uinfo-st3-tpl">
<div class="layui-row layui-col-space5">
	<div class="layui-col-xs3"></div>	
	<div class="layui-col-xs6 layui-bg-gray" style="margin:50px 0 50px 0;">
		<div class="layui-card">
		  <div class="layui-card-header">{{ d.title}}</div>
		  <div class="layui-card-body">
			  <div class="layui-row layui-col-space15">
				  <div class="layui-col-xs4">
					{{#  if(d.avatar != ''){ }}
					<img class="group-avatar" src="<?php _e(EYOUNG_CHAT_URL); ?>/images/group_logo.png">
					{{#  } else { }}
					<img class="group-avatar" src="<?php _e(EYOUNG_CHAT_URL); ?>/images/group_logo.png">
					{{#  } }}  
				  </div>
				  <div class="layui-col-xs8 list-intro">
					<form class="layui-form">
						<input type="hidden" name="rid" value="{{ d.rid }}">
						<div class="fox-form">
							<div class="layui-form-item">
									<input class="layui-input" name="apply" lay-verify="required" autocomplete="off" placeholder="请输入入群理由" type="text">
							</div>
							<div class="form-group">
								<div class="field">
									<button class="layui-btn layui-btn-primary"  style="width:100%;" lay-submit lay-filter="submitJoin3"><i class="layui-icon layui-icon-ok"></i> 申 请</button>
								</div>
							</div>
						</div>
					</form>
				  </div>
			  </div>
		  </div>
		</div>
	</div>
</div>
</script>
<script type="text/html" id="room-frame-tpl">
<div class="layui-row layui-col-space5 room-frame">
	<div class="layui-col-xs8 room-center">
		<div class="layui-row">
			<div class="layui-col-xs12" style="text-align:right;">
			 <div class="layui-btn-group">
				<button type="button" class="layui-btn layui-btn-primary layui-btn-xs" lay-active="toolsBullet"><i class="layui-icon layui-icon-ok-circle"></i>弹幕</button>
				<button type="button" class="layui-btn layui-btn-primary layui-btn-xs" lay-active="toolsHistory"><i class="layui-icon layui-icon-log"></i>历史</button>
				<button type="button" class="layui-btn layui-btn-primary layui-btn-xs" lay-active="toolsClear"><i class="layui-icon layui-icon-delete"></i>清屏</button>
				<button type="button" class="layui-btn layui-btn-primary layui-btn-xs" lay-active="toolsScroll"><i class="layui-icon layui-icon-ok-circle"></i>回滚</button>
			</div>
			</div>
		</div>
		<div class="chat-cont">
		<ul class="chat-list">
		</ul>
		</div>
		<div class="layui-row act-area layui-col-space5">
			<div class="layui-col-xs12">
				<div class="layui-btn-group">
				{{# if (d.base.faceopen=='1') { }}<button type="button" class="layui-btn layui-btn-primary layui-btn-disabled layui-btn-xs" lay-active="toolsFace" ><i class="layui-icon layui-icon-face-smile"></i>表情</button>{{# } }}
				{{# if (d.base.imageupload=='1') { }}<button type="button" class="layui-btn layui-btn-primary layui-btn-disabled layui-btn-xs" lay-active="toolsImage"><i class="layui-icon layui-icon-picture"></i>图片</button>{{# } }}
				{{# if (d.base.fileupload=='1') { }}<button type="button" class="layui-btn layui-btn-primary layui-btn-disabled layui-btn-xs" lay-active="toolsFile"><i class="layui-icon layui-icon-file"></i>文件</button>{{# } }}
				{{# if (d.uinfo.lvl== EY_Chat.comm.manager) { }}<button type="button" class="layui-btn layui-btn-primary layui-btn-disabled layui-btn-xs" lay-active="toolsManage"><i class="layui-icon layui-icon-set"></i>管理</button>{{# } }}
				</div>
			</div>
			<div class="layui-col-xs9">
				<textarea name="content" lay-active="setContent" disabled="true" placeholder="{lang EY_Chat:content_tip}" class="layui-textarea layui-btn-disabled chat-input"></textarea>
			</div>
			<div class="layui-col-xs3">
				<button type="button" class="layui-btn layui-btn-primary layui-btn-disabled chat-send" lay-active="toolsSend" style="height:54px;"><i class="layui-icon layui-icon-release" style="font-size: 30px;"></i> 发 送</button>
			</div>
			{{# if (d.base.limitspeed!='0') { }}
			<div class="layui-col-xs9">
				<div class="layui-progress" lay-filter="limitspeed"><div class="layui-progress-bar" lay-percent="0%"></div></div>
			</div>
			{{# } }}
		</div>
	</div>
	<div class="layui-col-xs4 room-right">
	{{# 
		var formatNotice = function(d){
			return d.replace(/\r\n/g, "<br/>").replace(/\r/g, "<br/>").replace(/\n/g, "<br/>");
		};
	}}
		<fieldset class="layui-elem-field room-notice" {{# if (!!d.base.notice && (d.base.notice != '')) { }} '' {{# }else{ }} style="display:none;" {{# } }} > 
			<legend>公告</legend>
			<div class="layui-field-box">
			{{ formatNotice(d.base.notice) }}
			</div>
		</fieldset>
	
		<div class="layui-tab" style="margin-top:0px;">
			<ul class="layui-tab-title">
				<li class="layui-this"><i class="layui-icon layui-icon-user"></i> 在线<span class="layui-badge layui-bg-gray online-numb">0</span></li>
			</ul>
			<div class="layui-tab-content">
				<div class="layui-tab-item layui-show">
					<div class="layui-row layui-col-space5 member-online">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</script>
<script type="text/html" id="memberitem-tpl">
	<div class="layui-col-xs3 member-item" uid="{{ d.uid }}" name="{{ d.name }}" lvl="{{ d.lvl }}">
		<div class="layui-panel"><div class="member-avatar"><img src="{{d.avatar}}"></div><div class="member-name">{{ d.name }}</div></div>
	</div>
</script>
<script type="text/html" id="chatitem-tpl">
	{{# if (!!d.me) { }}
	<li class="chat-item-me" cid="{{d.cid}}" time="{{d.ctime}}" style="display:none;">
		<div class="items-content">
		<p class="itemes-author">{{d.name}}</p>
		<div class="items-msg items-text"><span>{{d.content}}</span></div>
		</div>
		<a class="items-avatar" uid="{{d.uid}}" name="{{d.name}}" lvl="{{d.lvl}}"><img src="{{d.avatar}}"></a>
	</li>
	{{# }else{ }}
	<li class="chat-item" cid="{{d.cid}}" time="{{d.ctime}}" style="display:none;">
		<a class="items-avatar" uid="{{d.uid}}" name="{{d.name}}" lvl="{{d.lvl}}"><img src="{{d.avatar}}"></a>
		<div class="items-content">
			<p class="itemes-author">{{d.name}}</p>
			<div class="items-msg items-text"><span>{{d.content}}</span></div>
		</div>
	</li>
	{{# } }}
</script>
<script>
SITEURL = '<?php _e(site_url())?>';
EY_Chat = {};
EY_Chat.plugurl = '<?php _e(EYOUNG_CHAT_URL); ?>';
EY_Chat.plugjs = '<?php _e(EYOUNG_CHAT_JS); ?>';
EY_Chat.ajxurl = '<?php _e(admin_url('admin-ajax.php')); ?>';
EY_Chat.formhash = '<?php _e(wp_create_nonce()) ?>';

EY_Chat.comm = {};
EY_Chat.comm.socket = '<?php _e($setting['socket']); ?>';
EY_Chat.comm.v = "<?php _e($check);?>";
EY_Chat.lg = <?php _e(json_encode($lang)); ?>;
EY_Chat.roomIndex = 0;
EY_Chat.roomPage = new Array();

EY_Chat.roomBase = new Array();
EY_Chat.roomBase[0] = '';
EY_Chat.roomConf = new Array();
EY_Chat.roomConf[0] = <?php _e(json_encode($setting)); ?>;
EY_Chat.roomChat = new Array();
EY_Chat.roomChat[0] = <?php _e(json_encode($chat)); ?>;

EY_Chat.uinfo = new Array();
EY_Chat.uinfo[0] = <?php _e(json_encode($uinfo));?>;
layui.config({version:false,base:"<?=EYOUNG_CHAT_URL.EYOUNG_CHAT_JS?>/"}).use("chat");
</script>
</body>
</html>