<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body>
<div class="layui-container">  
<div class="layui-tab layui-tab-brief" lay-filter="roomFrame">
	<ul class="layui-tab-title">
		<li lay-id="roomSetting" class="layui-this">设置</li>
		<li lay-id="roomMember"">成员管理</li>
		<li lay-id="roomChat"">聊天管理</li>
	</ul>
	<div class="layui-tab-content" style="height: 100px;">
		<div class="layui-tab-item layui-show">
			<form class="layui-form" action="" lay-filter="roomSettingForm" id="roomSettingForm">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">聊天室名称</label>
						<div class="layui-input-inline">
							<input type="text" name="title" lay-verify="required" autocomplete="off" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux"></div>
					</div>
				</div>
				<div class="layui-form-item layui-form-text">
					<label class="layui-form-label">公告</label>
					<div class="layui-input-block">
					  <textarea name="notice" placeholder="公告信息" class="layui-textarea" style="width:480px;"></textarea>
					</div>
				</div>				
				<div class="layui-form-item" id="joinsetting"> <label class="layui-form-label">入群设置</label>
					<div class="layui-input-block">
						<input type="radio"  lay-filter="join"  name="join" value="0" title="开放入群" checked="">
						<input type="radio"  lay-filter="join"  name="join" value="1" title="用户组入群">
						<input type="radio"  lay-filter="join"  name="join" value="2" title="按口令入群">
						<input type="radio"  lay-filter="join"  name="join" value="3" title="群主审核">
					</div>
				</div>
				<div class="layui-form-item" id="join1Div" style="display:none;">
					<label class="layui-form-label"></label>
					<div class="layui-input-block">
						<div id="joingroup"></div>
					</div>
				</div>
				<div class="layui-form-item" id="join2Div" style="display:none;">
					<div class="layui-inline">
						<label class="layui-form-label">入群口令</label>
						<div class="layui-input-inline" style="width:100px;">
							<input type="text" name="join2" autocomplete="off" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux">请输入入群的口令</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">口令有效期</label>
						<div class="layui-input-inline" style="width:50px;">
							<input type="text" name="join2circle" lay-verify="number" autocomplete="off" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux">单位:小时</div>
					</div>
				</div>
				<div class="layui-form-item"> <label class="layui-form-label">发言设置</label>
					<div class="layui-input-block">
						<input type="radio" lay-filter="allowtalk" name="allowtalk" value="0" title="完全发言" checked="">
						<input type="radio" lay-filter="allowtalk" name="allowtalk" value="1" title="用户组发言">
						<input type="radio" lay-filter="allowtalk" name="allowtalk" value="2" title="禁言">
					</div>
				</div>
				<div class="layui-form-item" id="allowtalk1Div" style="display:none;">
					<label class="layui-form-label"></label>
					<div class="layui-input-block">
						 <div id="allowtalkgroup"></div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline"> <label class="layui-form-label">记录聊天内容</label>
						<div class="layui-input-block">
							<input type="checkbox" name="savechat" lay-skin="switch" lay-text="ON|OFF">
						</div>
					</div>
					<div class="layui-inline"> <label class="layui-form-label">默认显示聊天</label>
						<div class="layui-input-block">
							<input type="checkbox" name="chatshow" lay-skin="switch" lay-text="ON|OFF">
						</div>
					</div>
				</div>

				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">发言字数限制</label>
						<div class="layui-input-inline">
							<input type="text" name="limitcontent" lay-verify="number" autocomplete="off" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux"></div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">发言频率限制</label>
						<div class="layui-input-inline">
							<input type="text" name="limitspeed" lay-verify="number" autocomplete="off" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux">间隔秒数,0为不设限制</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline"><label class="layui-form-label">表情启用</label>
						<div class="layui-input-block">
							<input type="checkbox" name="faceopen" lay-skin="switch" lay-text="ON|OFF">
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline"><label class="layui-form-label">图片上传</label>
						<div class="layui-input-block">
							<input type="checkbox" name="imageupload" lay-skin="switch" lay-text="ON|OFF">
						</div>
					</div>	
					<div class="layui-inline"> 
						<label class="layui-form-label" style="width:60px;">上传大小</label>
						<div class="layui-input-inline"  style="width:60px;">
							<input type="text" name="imagelimit" lay-verify="number" autocomplete="off" class="layui-input" style="width:60px;">
						</div>
						<div class="layui-form-mid layui-word-aux">单位:KB</div>
					</div>
					<div class="layui-inline"> 
						<input type="checkbox" name="imagenotvisitor" title="限制游客上传">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline"><label class="layui-form-label">文件上传</label>
						<div class="layui-input-block">
							<input type="checkbox" name="fileupload" lay-skin="switch" lay-text="ON|OFF">
						</div>
					</div>	
					<div class="layui-inline"> 
						<label class="layui-form-label" style="width:60px;">上传大小</label>
						<div class="layui-input-inline"  style="width:60px;">
							<input type="text" name="filelimit" lay-verify="number" autocomplete="off" class="layui-input" style="width:60px;">
						</div>
						<div class="layui-form-mid layui-word-aux">单位:KB</div>
					</div>
					<div class="layui-inline"> 
						<input type="checkbox" name="filenotvisitor" title="限制游客上传">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline"><label class="layui-form-label">上传类型</label>
						<div class="layui-input-inline">
							<input type="text" name="fileext"  autocomplete="off" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux">允许上传的文件类型</div>
					</div>
				</div>
				<div class="layui-form-item layui-form-text">
					<label class="layui-form-label">过滤词</label>
					<div class="layui-input-block">
					  <textarea name="maskwords" placeholder="请输入需要屏敝的过滤词，多个过滤词之间请用半角逗号分隔。" class="layui-textarea" style="width:480px;"></textarea>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">推流服务地址</label>
						<div class="layui-input-inline">
							<input type="text" name="socket" lay-verify="required" autocomplete="off" class="layui-input">
						</div>
						<div class="layui-form-mid layui-word-aux">迸发人数(聊天室内最多在线人数)：<span id="online"><?php echo $_opt['online']?></span>人。</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">推流有效期</label>
						<div class="layui-input-inline" style="max-width:5px;"></div>
						<div class="layui-form-mid layui-word-aux">至：<span id="limitdate"><?php echo date('Y-m-d H:i', $_opt['limitdate'])?></span>。  &nbsp; <a href="<?php echo EYOUNGCHAT_Admin::$api_pay?>" target="_blank"><span style="color: blue;">延期&扩容</span></a>可扩大并发量和延长使用期限。<button type="button" class="layui-btn layui-btn-xs" id="resetsocket"><i class="layui-icon">&#xe669;</i> 刷新</button></div>
					</div>
				</div>
				 <div class="layui-form-item">
					<div class="layui-inline">
						<div class="layui-input-blocke"">
							<div class="layui-input-block">
								<button type="submit" class="layui-btn" lay-submit="" lay-filter="submitRoomSetting">确 定</button>
							</div>
						</div>
					</div>
				 </div>
			</form>
		</div>
		<div class="layui-tab-item" style="padding:0 10px 0 10px;">
			<table id="memberlist" lay-filter="memberTable"></table>
		</div>
		<div class="layui-tab-item" style="padding:0 10px 0 10px;">
			<table id="chatlist" lay-filter="chatTable"></table>
		</div>
	</div>
</div> 
<script type="text/html" id="roomTools">
	<div class="layui-btn-container">
		<button class="layui-btn layui-btn-sm" lay-event="add"> <i class="layui-icon">&#xe613;</i>{lang ey_chatplus:add}{lang ey_chatplus:room}</button>
		<button class="layui-btn layui-btn-sm" lay-event="refresh"> <i class="layui-icon">&#xe669;</i> </button>
	</div>
</script>
<script type="text/html" id="memberAddDiv">
<form class="layui-form" action="" lay-filter="memberAddForm">
  <div class="layui-form-item layui-form-text" style="padding:10px;30px;10px;30px;">
      <textarea name="memberids" placeholder="请输入要添加群的用户ID，多个用户ID用半角逗号分隔。" lay-verify="required"  class="layui-textarea" style="width:360px;"></textarea>
  </div>
  <div class="layui-form-item">
		<label class="layui-form-label"></label>
		<div class="layui-input-block">
			<div class="layui-btn-group">
			  <button class="layui-btn" lay-submit lay-filter="submitMemberAdd">确 定</button>
			  <a class="layui-btn layui-btn-primary" onclick="closeMemberAdd()">关 闭</a>
			</div>
		</div>
  </div>
</form>
</script>
<script type="text/html" id="memberTools">
	<div class="layui-btn-container">
		<button class="layui-btn layui-btn-sm" lay-event="add"> <i class="layui-icon">&#xe66f;</i>添加成员</button>
		<button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="del"> <i class="layui-icon">&#xe640;</i>删除成员</button>
		<button class="layui-btn layui-btn-sm" lay-event="refresh"> <i class="layui-icon">&#xe669;</i> </button>
	</div>
</script>
<script type="text/html" id="chatTools">
	<div class="layui-btn-container">
		<button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="del"> <i class="layui-icon">&#xe640;</i>删除聊天</button>
		<button class="layui-btn layui-btn-sm" lay-event="refresh"> <i class="layui-icon">&#xe669;</i> </button>
		<button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="delall"> <i class="layui-icon">&#x1006;</i>清空所有聊天</button>
	</div>
</script>
<table id="roomlist" lay-filter="roomTable"></table>
<script>
SITEURL = "$_G['siteurl']";
EY_Chat = {};
EY_Chat.setting = <?php _e(json_encode($_opt)) ?>;
EY_Chat.formhash = '<?php _e(wp_create_nonce()) ?>';
EY_Chat.plugurl = '<?php _e(EYOUNG_CHAT_URL)?>';
EY_Chat.ajxurl = '<?php _e(admin_url('admin-ajax.php'))?>';
EY_Chat.group = <?php _e(json_encode($group)) ?>;
EY_Chat.lg = <?php _e(json_encode($lang)) ?>;
layui.config({version:1.13,base:"<?php echo EYOUNG_CHAT_URL.EYOUNG_CHAT_JS?>/"}).use("admin");
</script>
</div>
</body>
</html>